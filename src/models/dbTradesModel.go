package models

import (
	"database/sql"
	"math"
	"time"

	"github.com/go-sql-driver/mysql"
	"gitlab.com/rene.sonntag/golang-tradingstories/src/repositories"
)

// InsertTrades : insert trade
func InsertTrades(assetID int64, assetName sql.NullString, assetSymbol sql.NullString, tradingstoryID string, amountOfAssets float64, buyPriceOfAsset float64, buyReason string, stoplossPercent float64) int64 {

	// check if tradingstory is already invested
	var id int
	row := db.QueryRow("SELECT id FROM trades WHERE tradingstories_pid = ? AND sell_date IS NULL", tradingstoryID)
	err := row.Scan(&id)

	if err == sql.ErrNoRows {

		// buy_date
		buyDate := time.Now()

		result, err := db.Exec("INSERT INTO trades (asset_pid, asset_name, asset_symbol, tradingstories_pid, amount, buy_date, buy_price, buy_reason, stopploss_percent) VALUES (?,?,?,?,?,?,?,?,?)", assetID, assetName, assetSymbol, tradingstoryID, amountOfAssets, buyDate, buyPriceOfAsset, buyReason, stoplossPercent)

		if err != nil {
			panic(err)
		}

		id, err := result.LastInsertId()

		if err != nil {
			panic(err)
		}

		// set last start of TradingStory
		result, err = db.Exec("UPDATE tradingstories SET start_date = ?, is_invested = 1 WHERE id = ?", buyDate, tradingstoryID)

		if err != nil {
			panic(err)
		}

		return id
	}

	return 0
}

// SellTrades : sets sell data for trades
func SellTrades(tradeID string, sellReason string, sellPrice float64) int64 {

	// sell_date
	sellDate := time.Now()

	result, err := db.Exec("UPDATE trades SET sell_reason = ?, sell_price = ?, sell_date = ? WHERE id = ?", sellReason, sellPrice, sellDate, tradeID)

	if err != nil {
		panic(err)
	}

	rows, err := result.RowsAffected()

	if err != nil {
		panic(err)
	}

	// set tradingstory to not invested
	var tradingstoryID float64
	err = db.QueryRow("SELECT tradingstories_pid FROM trades WHERE id = ?", tradeID).Scan(&tradingstoryID)

	if err != nil {
		panic(err)
	}

	// set tradingstory to not invested
	_, err = db.Exec("UPDATE tradingstories SET is_invested = 0 WHERE id = ?", tradingstoryID)

	if err != nil {
		panic(err)
	}

	return rows
}

// Trades : struct for Trades
type Trades struct {
	ID                 int64
	AssetPid           sql.NullInt64
	AssetName          sql.NullString
	AssetSymbol        sql.NullString
	Amount             sql.NullFloat64
	BuyDate            mysql.NullTime
	BuyPrice           sql.NullFloat64
	BuyReason          sql.NullString
	SellDate           mysql.NullTime
	SellPrice          sql.NullFloat64
	SellReason         sql.NullString
	StopplossPercent   sql.NullString
	CurrentPrice       float64
	CurrentPerformance float64
}

// GetTradesByTradingstoryID : get all trades of given tradingstroryID
func GetTradesByTradingstoryID(tradingstroryID int) []Trades {

	rows, err := db.Query("SELECT id, asset_pid, asset_name, asset_symbol, amount, buy_date, buy_price, buy_reason, sell_date, sell_price, sell_reason, stopploss_percent FROM trades WHERE tradingstories_pid = ? ORDER BY id DESC", tradingstroryID)

	if err != nil {
		panic(err)
	}

	defer rows.Close()

	var resultArray []Trades

	for rows.Next() {

		trades := Trades{}

		err := rows.Scan(
			&trades.ID,
			&trades.AssetPid,
			&trades.AssetName,
			&trades.AssetSymbol,
			&trades.Amount,
			&trades.BuyDate,
			&trades.BuyPrice,
			&trades.BuyReason,
			&trades.SellDate,
			&trades.SellPrice,
			&trades.SellReason,
			&trades.StopplossPercent)

		if err != nil {
			panic(err)
		}

		// Performance berechnen (buyPrice -> SellPrice)

		var performancePrice float64

		if trades.SellPrice.Valid {

			performancePriceSellPrice, _ := trades.SellPrice.Value()
			performancePrice = performancePriceSellPrice.(float64)
		} else {

			// just running investment
			// get current bid price of asset from API

			q, err := repositories.GetQuoteFromAPI(trades.AssetSymbol.String)

			if err == false {
				performancePrice = q.Bid
			} else {
				performancePrice = 0
			}
		}

		buyPricy, _ := trades.BuyPrice.Value()
		currentPerformance := math.Round((100.00 / buyPricy.(float64) * performancePrice) - 100.00)

		// append resultArray
		resultArray = append(resultArray, Trades{
			ID:                 trades.ID,
			AssetPid:           trades.AssetPid,
			AssetName:          trades.AssetName,
			AssetSymbol:        trades.AssetSymbol,
			Amount:             trades.Amount,
			BuyDate:            trades.BuyDate,
			BuyPrice:           trades.BuyPrice,
			BuyReason:          trades.BuyReason,
			SellDate:           trades.SellDate,
			SellPrice:          trades.SellPrice,
			SellReason:         trades.SellReason,
			StopplossPercent:   trades.StopplossPercent,
			CurrentPerformance: currentPerformance,
			CurrentPrice:       performancePrice})

		if err != nil {
			panic(err)
		}
	}

	err = rows.Err()

	if err != nil {
		panic(err)
	}

	return resultArray
}

// GetSymbolByTradeID : get symbol of given trade
func GetSymbolByTradeID(tradeID string) (string, bool) {

	var assetSymbol string

	err := db.QueryRow(`
		SELECT 
		assets.symbol
		FROM trades 
		LEFT JOIN assets
		ON assets.id = trades.asset_pid
		WHERE trades.id = ? AND sell_price IS NULL 
		LIMIT 1`, tradeID).Scan(&assetSymbol)

	if err != nil {
		return "", false
	}

	return assetSymbol, true
}
