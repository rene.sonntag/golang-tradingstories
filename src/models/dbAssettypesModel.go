package models

import (
	"database/sql"
)

// AssetTypes : struct for assettypes
type AssetTypes struct {
	ID   int64
	Name string
}

// GetAssetTypes : get all assettypes
func GetAssetTypes() []AssetTypes {

	rows, err := db.Query("SELECT id, name FROM asset_types")

	if err != nil {
		panic(err)
	}

	defer rows.Close()

	var resultArray []AssetTypes

	for rows.Next() {

		assetTypes := AssetTypes{}

		err := rows.Scan(&assetTypes.ID, &assetTypes.Name)
		if err != nil {
			panic(err)
		}

		resultArray = append(resultArray, AssetTypes{ID: assetTypes.ID, Name: assetTypes.Name})

		if err != nil {
			panic(err)
		}
	}

	err = rows.Err()

	if err != nil {
		panic(err)
	}

	return resultArray
}

// InsertNewAssetTypes : insert new assettype
func InsertNewAssetTypes(name string) int64 {

	var id int
	row := db.QueryRow("SELECT id FROM asset_types WHERE name = ?", name)
	err := row.Scan(&id)

	if err == sql.ErrNoRows {

		result, err := db.Exec("INSERT INTO asset_types (name) VALUES (?)", name)

		if err != nil {
			panic(err)
		}

		id, err := result.LastInsertId()

		if err != nil {
			panic(err)
		}

		return id
	}

	return 0
}

// DeleteAssetTypes : delete assettype
func DeleteAssetTypes(assettypeID string) int64 {

	var id int
	row := db.QueryRow("SELECT id FROM asset_types WHERE id = ?", assettypeID)
	err := row.Scan(&id)

	if err == sql.ErrNoRows {

		return 0
	}

	_, err = db.Exec("DELETE FROM asset_types WHERE id = ?", assettypeID)

	if err != nil {
		panic(err)
	}

	return 1
}
