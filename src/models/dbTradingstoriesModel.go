package models

import (
	"database/sql"
	"math"

	"github.com/go-sql-driver/mysql"
	"gitlab.com/rene.sonntag/golang-tradingstories/src/repositories"
)

// Tradingstories : struct for Tradingstories
type Tradingstories struct {
	ID                 int64
	Name               string
	Description        string
	Type               string
	TrailingDistance   int64
	TradingVolume      string
	StartDate          mysql.NullTime
	IsInvested         sql.NullInt64
	AssetTypeNames     []AssetTypeNames
	GeneralPerformance float64
	CurrentPerformance float64
}

// AssetTypeNames : struct for AssetTypeNames
type AssetTypeNames struct {
	ID   int64
	Name string
}

// GetTradingstories : get all tradingstories
func GetTradingstories() []Tradingstories {

	rows, err := db.Query("SELECT id, name, description, type, trailing_distance, trading_volume, start_date, is_invested FROM tradingstories")

	if err != nil {
		panic(err)
	}

	defer rows.Close()

	var resultArray []Tradingstories

	for rows.Next() {

		tradingstories := Tradingstories{}

		err := rows.Scan(
			&tradingstories.ID,
			&tradingstories.Name,
			&tradingstories.Description,
			&tradingstories.Type,
			&tradingstories.TrailingDistance,
			&tradingstories.TradingVolume,
			&tradingstories.StartDate,
			&tradingstories.IsInvested)

		if err != nil {
			panic(err)
		}

		// get names of assetType and append it to resultArray
		assetTypeRows, err := db.Query("SELECT asset_types.id, asset_types.name FROM asset_types INNER JOIN tradingstories_assettypes_mm ON asset_types.id = tradingstories_assettypes_mm.assettypes_pid WHERE tradingstories_assettypes_mm.tradingstories_pid = ?", tradingstories.ID)
		if err != nil {
			panic(err)
		}

		var resultArrayAssetTypeNames []AssetTypeNames
		for assetTypeRows.Next() {
			assetTypeNames := AssetTypeNames{}
			err := assetTypeRows.Scan(&assetTypeNames.ID, &assetTypeNames.Name)
			if err != nil {
				panic(err)
			}
			resultArrayAssetTypeNames = append(resultArrayAssetTypeNames, AssetTypeNames{ID: assetTypeNames.ID, Name: assetTypeNames.Name})
		}

		// get general performance and append it to resultArray
		// get performance of old trades

		generalPerformanceRows, err := db.Query("SELECT 100/buy_price*sell_price-100 FROM trades WHERE sell_date IS NOT NULL AND tradingstories_pid = ?", tradingstories.ID)
		if err != nil {
			panic(err)
		}

		// Geometrisches Mittel
		// Einzelrenditen + 1 rechnen (Bsp 37%: 0.37 + 1 = 1.37)
		// Einzelwerte multiplizieren
		// n-te Wurzel auf Ergebnis

		var generalPerformance float64
		var countTrades int
		var tradePerformance sql.NullFloat64
		var tempPerformance float64
		var tempSingleValuesMultiplied float64

		countTrades = 0

		for generalPerformanceRows.Next() {

			err := generalPerformanceRows.Scan(&tradePerformance)
			if err != nil {
				panic(err)
			}

			tempPerformance = (tradePerformance.Float64 / 100.00) + 1.00

			if countTrades > 0 {
				tempSingleValuesMultiplied = tempSingleValuesMultiplied * tempPerformance
			} else {
				tempSingleValuesMultiplied = tempPerformance
			}

			countTrades++
		}

		if countTrades == 1 {

			generalPerformance = tradePerformance.Float64
		} else {
			if countTrades > 0 {

				generalPerformance = root(tempSingleValuesMultiplied, countTrades) * 10
			} else {

				generalPerformance = 0.0
			}
		}

		// round generalPerformance to 2 decimals
		generalPerformance = math.Round(generalPerformance*100) / 100

		// get performance of current trade
		var buyPrice float64
		var assetSymbol string

		err = db.QueryRow(`
			SELECT 
			trades.buy_price, assets.symbol
			FROM trades 
			LEFT JOIN assets
			ON assets.id = trades.asset_pid
			WHERE tradingstories_pid = ? AND sell_price IS NULL 
			LIMIT 1`, tradingstories.ID).Scan(&buyPrice, &assetSymbol)

		if err != nil {
			buyPrice = 0.0
		}

		// get current price of asset from API
		var currentPrice float64

		if assetSymbol != "" {

			q, err := repositories.GetQuoteFromAPI(assetSymbol)

			if err == false {
				currentPrice = q.Bid
			} else {
				currentPrice = 0.0
			}
		} else {
			currentPrice = 0.0
		}

		var currentPerformance float64
		if buyPrice > 0.0 {

			currentPerformance = math.Round(100/buyPrice*currentPrice - 100)
		} else {

			currentPerformance = 0.0
		}

		// append performances to resultArray
		resultArray = append(resultArray, Tradingstories{
			ID:                 tradingstories.ID,
			Name:               tradingstories.Name,
			Description:        tradingstories.Description,
			Type:               tradingstories.Type,
			TrailingDistance:   tradingstories.TrailingDistance,
			TradingVolume:      tradingstories.TradingVolume,
			StartDate:          tradingstories.StartDate,
			IsInvested:         tradingstories.IsInvested,
			AssetTypeNames:     resultArrayAssetTypeNames,
			GeneralPerformance: generalPerformance,
			CurrentPerformance: currentPerformance})
	}

	err = rows.Err()

	if err != nil {
		panic(err)
	}

	return resultArray
}

func root(a float64, n int) float64 {

	if a > 0.0 {
		n1 := n - 1
		n1f, rn := float64(n1), 1/float64(n)
		x, x0 := 1., 0.
		for {
			potx, t2 := 1/x, a
			for b := n1; b > 0; b >>= 1 {
				if b&1 == 1 {
					t2 *= potx
				}
				potx *= potx
			}
			x0, x = x, rn*(n1f*x+t2)
			if math.Abs(x-x0)*1e15 < x {
				break
			}
		}
		return x
	}

	return 0
}

// GetTradingstoryByID : get data of tradingstory by given tradingstoriesID
func GetTradingstoryByID(tradingstoriesID string) []Tradingstories {

	rows, err := db.Query("SELECT id, name, description, type, trailing_distance, trading_volume, start_date, is_invested FROM tradingstories WHERE id = ?", tradingstoriesID)

	if err != nil {
		panic(err)
	}

	defer rows.Close()

	var resultArray []Tradingstories

	for rows.Next() {

		tradingstories := Tradingstories{}

		err := rows.Scan(
			&tradingstories.ID, 
			&tradingstories.Name, 
			&tradingstories.Description, 
			&tradingstories.Type, 
			&tradingstories.TrailingDistance, 
			&tradingstories.TradingVolume, 
			&tradingstories.StartDate, 
			&tradingstories.IsInvested)
		
		if err != nil {
			panic(err)
		}

		// get names of assetType and append it to resultArray
		assetTypeRows, err := db.Query("SELECT asset_types.id, asset_types.name FROM asset_types INNER JOIN tradingstories_assettypes_mm ON asset_types.id = tradingstories_assettypes_mm.assettypes_pid WHERE tradingstories_assettypes_mm.tradingstories_pid = ?", tradingstories.ID)
		if err != nil {
			panic(err)
		}

		var resultArrayAssetTypeNames []AssetTypeNames
		for assetTypeRows.Next() {
			assetTypeNames := AssetTypeNames{}
			err := assetTypeRows.Scan(&assetTypeNames.ID, &assetTypeNames.Name)
			if err != nil {
				panic(err)
			}
			resultArrayAssetTypeNames = append(resultArrayAssetTypeNames, AssetTypeNames{ID: assetTypeNames.ID, Name: assetTypeNames.Name})
		}

		// append resultArray
		resultArray = append(resultArray, Tradingstories{ID: tradingstories.ID, Name: tradingstories.Name, Description: tradingstories.Description, Type: tradingstories.Type, TrailingDistance: tradingstories.TrailingDistance, TradingVolume: tradingstories.TradingVolume, StartDate: tradingstories.StartDate, IsInvested: tradingstories.IsInvested, AssetTypeNames: resultArrayAssetTypeNames})

		if err != nil {
			panic(err)
		}
	}

	err = rows.Err()

	if err != nil {
		panic(err)
	}

	return resultArray
}

// InsertNewTradingstories : insert tradingstory
func InsertNewTradingstories(name string, description string, assettypePid []string, tradingstoriesType string, trailingDistance string, tradingVolume string) int64 {

	var id int
	row := db.QueryRow("SELECT id FROM tradingstories WHERE name = ?", name)
	err := row.Scan(&id)

	if err == sql.ErrNoRows {

		// insert into table tradingstories
		result, err := db.Exec("INSERT INTO tradingstories (name, description, type, trailing_distance, trading_volume_start, trading_volume, is_invested) VALUES (?,?,?,?,?,?,?)", name, description, tradingstoriesType, trailingDistance, tradingVolume, tradingVolume, 0)

		if err != nil {
			panic(err)
		}

		id, err := result.LastInsertId()

		if err != nil {
			panic(err)
		}

		// delete former entries in tradingstories_assettypes_mm for new id
		_, err = db.Exec("DELETE FROM tradingstories_assettypes_mm WHERE tradingstories_pid = ?", id)

		// insert into table tradingstories_assettypes_mm
		for _, s := range assettypePid {
			_, err = db.Exec("INSERT INTO tradingstories_assettypes_mm (tradingstories_pid, assettypes_pid) VALUES (?,?)", id, s)
		}

		return id
	}

	return 0
}

// UpdateTradingstories : update tradingstory
func UpdateTradingstories(id string, name string, description string, assettypePid []string, tradingstoriesType string, trailingDistance string, tradingVolume string) int64 {

	// insert into table tradingstories
	_, err := db.Exec("UPDATE tradingstories SET name = ?, description = ?, type = ?, trailing_distance = ?, trading_volume = ? WHERE id = ?", name, description, tradingstoriesType, trailingDistance, tradingVolume, id)

	if err != nil {
		panic(err)
	}

	// delete former entries in tradingstories_assettypes_mm for new id
	_, err = db.Exec("DELETE FROM tradingstories_assettypes_mm WHERE tradingstories_pid = ?", id)

	if err != nil {
		panic(err)
	}

	// insert into table tradingstories_assettypes_mm
	for _, s := range assettypePid {
		_, err = db.Exec("INSERT INTO tradingstories_assettypes_mm (tradingstories_pid, assettypes_pid) VALUES (?,?)", id, s)
	}

	if err != nil {
		panic(err)
	} else {

		return 1
	}
}
