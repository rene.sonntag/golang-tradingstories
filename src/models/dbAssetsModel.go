package models

import (
	"database/sql"
	"fmt"

	"gitlab.com/rene.sonntag/golang-tradingstories/src/repositories"
)

// Assets : struct for assets
type Assets struct {
	ID            int64
	AssettypePid  string
	AssettypeName string
	Name          string
	Symbol        string
	Disabled      string
}

// GetAssets : get all assets
func GetAssets() []Assets {

	rows, err := db.Query("SELECT assets.id AS id, assettype_pid, asset_types.name AS assettype_name, assets.name, symbol, disabled FROM assets INNER JOIN asset_types ON asset_types.id = assets.assettype_pid")

	if err != nil {
		panic(err)
	}

	defer rows.Close()

	var resultArray []Assets

	for rows.Next() {

		assets := Assets{}

		err := rows.Scan(&assets.ID, &assets.AssettypePid, &assets.AssettypeName, &assets.Name, &assets.Symbol, &assets.Disabled)
		if err != nil {
			panic(err)
		}

		resultArray = append(resultArray, Assets{ID: assets.ID, AssettypePid: assets.AssettypePid, AssettypeName: assets.AssettypeName, Name: assets.Name, Symbol: assets.Symbol, Disabled: assets.Disabled})

		if err != nil {
			panic(err)
		}
	}

	err = rows.Err()

	if err != nil {
		panic(err)
	}

	return resultArray
}

// AssetsByTradingsstory : struct for AssetsByTradingsstory
type AssetsByTradingsstory struct {
	ID   int64
	Name string
}

// GetAssetsByTradingstoryID : get all assets for given tradingstoryID
func GetAssetsByTradingstoryID(tradingstoryID string) []AssetsByTradingsstory {

	// get assets by tradingstoryID
	rows, err := db.Query("SELECT DISTINCT a.id, a.name, a.symbol FROM assets AS a INNER JOIN asset_types AS at ON at.id = a.assettype_pid INNER JOIN tradingstories_assettypes_mm as mm on mm.assettypes_pid = at.id WHERE mm.tradingstories_pid = ?", tradingstoryID)

	if err != nil {
		panic(err)
	}

	defer rows.Close()

	var resultArray []AssetsByTradingsstory

	var symbol string

	for rows.Next() {

		assets := AssetsByTradingsstory{}

		err := rows.Scan(&assets.ID, &assets.Name, &symbol)
		if err != nil {
			panic(err)
		}

		// get current buy price from API
		var currentBuyPrice float64
		var currencyID string

		if symbol != "" {

			q, err := repositories.GetQuoteFromAPI(symbol)

			currencyID = q.CurrencyID

			if err == false {
				currentBuyPrice = q.Bid
			} else {
				currentBuyPrice = 0.0
			}
		} else {
			currentBuyPrice = 0.0
		}

		resultArray = append(resultArray, AssetsByTradingsstory{
			ID:   assets.ID,
			Name: assets.Name + " (" + fmt.Sprintf("%.2f", currentBuyPrice) + " " + currencyID + ")"})

		if err != nil {
			panic(err)
		}
	}

	err = rows.Err()

	if err != nil {
		panic(err)
	}

	return resultArray
}

// AssetData : struct for single asset
type AssetData struct {
	ID           sql.NullInt64
	AssettypePid sql.NullInt64
	Name         sql.NullString
	Symbol       sql.NullString
	Disabled     sql.NullInt64
}

// GetAssetDataByAssetID : get asset data for given assetID
func GetAssetDataByAssetID(assetID int64) []AssetData {

	// get name, symbol of asset
	rows, err := db.Query("SELECT id, assettype_pid, name, symbol, disabled FROM assets WHERE id = ?", assetID)

	if err != nil {
		panic(err)
	}

	defer rows.Close()

	var resultArray []AssetData

	for rows.Next() {

		asset := AssetData{}

		err := rows.Scan(&asset.ID, &asset.AssettypePid, &asset.Name, &asset.Symbol, &asset.Disabled)
		if err != nil {
			panic(err)
		}

		resultArray = append(resultArray, AssetData{ID: asset.ID, AssettypePid: asset.AssettypePid, Name: asset.Name, Symbol: asset.Symbol, Disabled: asset.Disabled})

		if err != nil {
			panic(err)
		}
	}

	err = rows.Err()

	if err != nil {
		panic(err)
	}

	return resultArray
}

// InsertNewAssets : insert new asset
func InsertNewAssets(assettypePid string, name string, symbol string, disabled string) int64 {

	var id int
	row := db.QueryRow("SELECT id FROM assets WHERE symbol = ?", symbol)
	err := row.Scan(&id)

	if err == sql.ErrNoRows {

		result, err := db.Exec("INSERT INTO assets (assettype_pid, name, symbol, disabled) VALUES (?,?,?,?)", assettypePid, name, symbol, disabled)

		if err != nil {
			panic(err)
		}

		id, err := result.LastInsertId()

		if err != nil {
			panic(err)
		}

		return id
	}

	return 0
}

// DeleteAssets : delete asset
func DeleteAssets(assetID string) int64 {

	var id int
	row := db.QueryRow("SELECT id FROM assets WHERE id = ?", assetID)
	err := row.Scan(&id)

	if err == sql.ErrNoRows {

		return 0
	}

	_, err = db.Exec("DELETE FROM assets WHERE id = ?", assetID)

	if err != nil {
		panic(err)
	}

	return 1
}

// ActivateAssets : activate asset
func ActivateAssets(assetID string) int64 {

	var id int
	row := db.QueryRow("SELECT id FROM assets WHERE id = ?", assetID)
	err := row.Scan(&id)

	if err == sql.ErrNoRows {

		return 0
	}

	_, err = db.Exec("UPDATE assets SET disabled = 0 WHERE id = ?", assetID)

	if err != nil {
		panic(err)
	}

	return 1
}

// DeactivateAssets : activate asset
func DeactivateAssets(assetID string) int64 {

	var id int
	row := db.QueryRow("SELECT id FROM assets WHERE id = ?", assetID)
	err := row.Scan(&id)

	if err == sql.ErrNoRows {

		return 0
	}

	_, err = db.Exec("UPDATE assets SET disabled = 1 WHERE id = ?", assetID)

	if err != nil {
		panic(err)
	}

	return 1
}
