package controllers

import (
	"database/sql"
	"html/template"
	"math"
	"net/http"
	"strconv"

	"gitlab.com/rene.sonntag/golang-tradingstories/src/models"
	"gitlab.com/rene.sonntag/golang-tradingstories/src/repositories"
)

// DataTradingstoriesManuell : struct for tradingstoriesManuell
type DataTradingstoriesManuell struct {
	Title                   string
	Subtitle                string
	ActiveMenu              string
	FormManuellMessage      string
	FormManuellMessageError string
	FormDeleteMessage       string
	FormDeleteMessageError  string
	Assets                  []models.AssetsByTradingsstory
	TradingstoryIsInvested  sql.NullInt64
	ID                      int64
	Name                    string
	Description             string
	AssetTypeNames          []models.AssetTypeNames
	Type                    string
	TrailingDistance        int64
	TradingVolume           string
}

// TradingstoriesManuellHandler : handler for manuell trading of tradingstorie
func TradingstoriesManuellHandler(w http.ResponseWriter, r *http.Request) {

	var TradingstoriesManuellData DataTradingstoriesManuell

	// get manuell tradingstorie from request
	if r.Method == "POST" {

		// parse form
		r.ParseForm()

		// get assets of given tradingstory from database
		TradingstoriesManuellData.Assets = models.GetAssetsByTradingstoryID(r.FormValue("tradingstoryid"))

		// get tradingstory data from database
		tradingstoryByIDData := models.GetTradingstoryByID(r.FormValue("tradingstoryid"))
		TradingstoriesManuellData.ID = tradingstoryByIDData[0].ID
		TradingstoriesManuellData.Name = tradingstoryByIDData[0].Name
		TradingstoriesManuellData.Type = tradingstoryByIDData[0].Type
		TradingstoriesManuellData.TrailingDistance = tradingstoryByIDData[0].TrailingDistance
		TradingstoriesManuellData.TradingVolume = tradingstoryByIDData[0].TradingVolume
		TradingstoriesManuellData.TradingstoryIsInvested = tradingstoryByIDData[0].IsInvested

		if r.FormValue("submitManuellTradingstorie") != "" {

			// save new trade
			newTradeAssetID := r.FormValue("asset")
			newTradeTradingstoryID := r.FormValue("tradingstoryid")
			newTradeTradingReason := r.FormValue("trading_reason")

			newTradeTradingVolume, err := strconv.ParseFloat(r.FormValue("trading_volume"), 64)
			if err != nil {
				panic(err)
			}

			newTradeTrailingDistance, err := strconv.ParseFloat(r.FormValue("trailing_distance"), 64)
			if err != nil {
				panic(err)
			}

			var insertedID int64

			if newTradeAssetID != "" && newTradeTradingstoryID != "" && newTradeTrailingDistance > 0 && newTradeTradingVolume >= 0 && newTradeTradingReason != "" {

				newTradeAssetID, err := strconv.ParseInt(newTradeAssetID, 10, 64)
				if err != nil {
					panic(err)
				}
				AssetData := models.GetAssetDataByAssetID(newTradeAssetID)

				// get ask price of asset from API
				q, error := repositories.GetQuoteFromAPI(AssetData[0].Symbol.String)

				var buyPriceOfAsset float64

				if error == false {
					buyPriceOfAsset = q.Ask
				} else {
					buyPriceOfAsset = 0
				}

				// Anzahl der Einheiten des Assets berechnen
				amountOfAssets := math.Floor(newTradeTradingVolume / buyPriceOfAsset)

				if amountOfAssets > 0 {

					newTradeTradingReason = "Manuell: " + newTradeTradingReason

					insertedID = models.InsertTrades(newTradeAssetID, AssetData[0].Name, AssetData[0].Symbol, newTradeTradingstoryID, amountOfAssets, buyPriceOfAsset, newTradeTradingReason, newTradeTrailingDistance)

					if insertedID == 0 {

						// error
						TradingstoriesManuellData.FormManuellMessageError = "Fehler beim Anlegen des Trades!"
					} else {

						// success
						TradingstoriesManuellData.FormManuellMessage = "Trades mit der ID " + strconv.Itoa(int(insertedID)) + " erfolgreich angelegt."
					}
				} else {

					// Trading Volume zu gering
					TradingstoriesManuellData.FormManuellMessageError = "Fehler: Das aktuelle Trading Volume der TradingStory ist zu gering."
				}
			} else {

				// error
				TradingstoriesManuellData.FormManuellMessageError = "Fehler: Pflichtfelder nicht befüllt!"
			}
		}
	}

	// set meta data
	TradingstoriesManuellData.Title = "Trading"
	TradingstoriesManuellData.Subtitle = "TradingStory manuell traden"
	TradingstoriesManuellData.ActiveMenu = "tradingstoriesmanuell"

	var templates = template.Must(template.ParseGlob("templates/Bootstrapious/templates/*"))

	err := templates.ExecuteTemplate(w, "tradingstoriesmanuell", TradingstoriesManuellData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
