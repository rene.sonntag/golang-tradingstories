package controllers

import (
	"html/template"
	"net/http"

	"gitlab.com/rene.sonntag/golang-tradingstories/src/models"
	"gitlab.com/rene.sonntag/golang-tradingstories/src/repositories"
)

// DataTradesManuellSell struct
type DataTradesManuellSell struct {
	Title                   string
	Subtitle                string
	ActiveMenu              string
	FormManuellMessage      string
	FormManuellMessageError string
	FormDeleteMessage       string
	FormDeleteMessageError  string
	ID                      string
	TradingstoryID          string
	SellPrice               float64
}

// TradingstoriesManuellSellHandler : Controller for manuell selling
func TradingstoriesManuellSellHandler(w http.ResponseWriter, r *http.Request) {

	var TradesManuellData DataTradesManuellSell

	// get manuell trade from request
	if r.Method == "POST" {

		// parse form
		r.ParseForm()

		TradesManuellData.ID = r.FormValue("tradeid")
		TradesManuellData.TradingstoryID = r.FormValue("tradingstoryid")

		// get current sell price from API
		assetSymbol, symbolValid := models.GetSymbolByTradeID(r.FormValue("tradeid"))

		var currentSellPrice float64

		if symbolValid == true {

			if assetSymbol != "" {

				q, err := repositories.GetQuoteFromAPI(assetSymbol)

				if err == false {
					currentSellPrice = q.Bid
				} else {
					currentSellPrice = 0.0
				}
			} else {
				currentSellPrice = 0.0
			}

			TradesManuellData.SellPrice = currentSellPrice

			if r.FormValue("submitGotoManuellSellConfirmTrades") != "" && r.FormValue("tradeid") != "" {

				// Sell trade
				sellReason := "Manueller Verkauf"

				affectedRows := models.SellTrades(r.FormValue("tradeid"), sellReason, currentSellPrice)

				if affectedRows == 0 {

					// error
					TradesManuellData.FormManuellMessageError = "Fehler beim Verkaufen des Assets!"
				} else {

					// success
					TradesManuellData.FormManuellMessage = "Asset wurde erfolgreich verkauft."
				}
			}
		} else {

			// error
			TradesManuellData.FormManuellMessageError = "Fehler beim Verkaufen des Assets!"
		}
	}

	// set meta data
	TradesManuellData.Title = "Asset verkaufen"
	TradesManuellData.Subtitle = "Asset manuell verkaufen"
	TradesManuellData.ActiveMenu = "tradingstoriesmanuellsell"

	var templates = template.Must(template.ParseGlob("templates/Bootstrapious/templates/*"))

	err := templates.ExecuteTemplate(w, "tradingstoriesmanuellsell", TradesManuellData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
