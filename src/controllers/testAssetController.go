package controllers

import (
	"fmt"
	"net/http"

	"gitlab.com/rene.sonntag/golang-tradingstories/src/repositories"
)

// TestAssetHandler : handler for testing an asset
func TestAssetHandler(w http.ResponseWriter, r *http.Request) {

	var resultString string

	if r.Method == "POST" {

		// parse form
		r.ParseForm()

		symbol := r.FormValue("ajax_post_data")

		if symbol != "" {

			// get asset data from API
			q, err := repositories.GetQuoteFromAPI(symbol)

			if err == false {
				resultString = q.ShortName + "###" + fmt.Sprintf("%f", q.Bid)
				w.Header().Set("Content-Type", "text/plain")
				w.WriteHeader(http.StatusOK)
				w.Write([]byte(resultString))
			} else {
				resultString = ""
				w.WriteHeader(http.StatusBadRequest)
			}
		} else {
			resultString = ""
			w.WriteHeader(http.StatusBadRequest)
		}
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}
}
