package controllers

import (
	"net/http"
	"html/template"

	"gitlab.com/rene.sonntag/golang-tradingstories/src/models"
)

// DataTradingstories : struct for tradingstories
type DataTradingstories struct {
	Title string
    Subtitle string
	ActiveMenu string
	FormNewMessage string
	FormNewMessageError string
	FormDeleteMessage string
	FormDeleteMessageError string
	Tradingstories []models.Tradingstories
}

// TradingstoriesHandler : handler for tradingstorie
func TradingstoriesHandler(w http.ResponseWriter, r *http.Request) {

	var TradingstoriesData DataTradingstories
	
	// get tradingstories from database
	TradingstoriesData.Tradingstories = models.GetTradingstories()

	// set meta data
	TradingstoriesData.Title = "TradingStories"
	TradingstoriesData.Subtitle = "Alle TradingStories"
	TradingstoriesData.ActiveMenu = "tradingstories"

	var templates = template.Must(template.ParseGlob("templates/Bootstrapious/templates/*"))	

	err := templates.ExecuteTemplate(w, "tradingstories", TradingstoriesData)

	if err != nil {
		 http.Error(w, err.Error(), http.StatusInternalServerError)
		 return
	}
}
