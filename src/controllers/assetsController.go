package controllers

import (
	"html/template"
	"net/http"
	"strconv"

	"gitlab.com/rene.sonntag/golang-tradingstories/src/models"
)

// DataAssets : struct for assets
type DataAssets struct {
	Title                  string
	Subtitle               string
	ActiveMenu             string
	FormNewMessage         string
	FormNewMessageError    string
	FormDeleteMessage      string
	FormDeleteMessageError string
	ResultArray            []models.Assets
	Assettypes             []models.AssetTypes
}

// AssetsHandler : handler for assets
func AssetsHandler(w http.ResponseWriter, r *http.Request) {

	var AssetsData DataAssets

	// get new assets from form
	if r.Method == "POST" {

		// parse form
		r.ParseForm()

		if r.FormValue("submitNewAsset") == "Speichern" {

			// save new asset
			newAssetAssettypePid := r.FormValue("assettype_pid")
			newAssetName := r.FormValue("name")
			newAssetSymbol := r.FormValue("symbol")
			newAssetDisabled := r.FormValue("disabled")

			var insertedID int64

			if newAssetName != "" && newAssetSymbol != "" {

				insertedID = models.InsertNewAssets(newAssetAssettypePid, newAssetName, newAssetSymbol, newAssetDisabled)

				if insertedID == 0 {

					// error
					AssetsData.FormNewMessageError = "Fehler beim Anlegen oder Asset bereits vorhanden!"
				} else {

					// success
					AssetsData.FormNewMessage = "Asset mit der ID " + strconv.Itoa(int(insertedID)) + " erfolgreich angelegt."
				}
			} else {

				// error
				AssetsData.FormNewMessageError = "Fehler: Pflichtfelder nicht befüllt"
			}
		}

		if r.FormValue("submitStatus") == "Aktivieren" {

			// delete asset
			assetID := r.FormValue("assetId")

			var deletedID int64

			if assetID != "" {

				deletedID = models.ActivateAssets(assetID)

				if deletedID == 0 {

					// error
					AssetsData.FormDeleteMessageError = "Fehler beim Aktivieren des Assets!"
				} else {

					// success
					AssetsData.FormDeleteMessage = "Asset mit der ID " + assetID + " erfolgreich aktiviert."
				}
			} else {

				// error
				AssetsData.FormDeleteMessageError = "Fehler beim Aktivieren des Assets!"
			}
		}

		if r.FormValue("submitStatus") == "Deaktivieren" {

			// delete asset
			assetID := r.FormValue("assetId")

			var deletedID int64

			if assetID != "" {

				deletedID = models.DeactivateAssets(assetID)

				if deletedID == 0 {

					// error
					AssetsData.FormDeleteMessageError = "Fehler beim Deaktivieren des Assets!"
				} else {

					// success
					AssetsData.FormDeleteMessage = "Asset mit der ID " + assetID + " erfolgreich deaktiviert."
				}
			} else {

				// error
				AssetsData.FormDeleteMessageError = "Fehler beim Deaktivieren des Assets!"
			}
		}
	}

	// get assets from database
	AssetsData.ResultArray = models.GetAssets()

	// get assettypes from database
	AssetsData.Assettypes = models.GetAssetTypes()

	// set meta data
	AssetsData.Title = "Asset-Pool"
	AssetsData.Subtitle = "Verwaltung von Assets"
	AssetsData.ActiveMenu = "assets"

	var templates = template.Must(template.ParseGlob("templates/Bootstrapious/templates/*"))

	err := templates.ExecuteTemplate(w, "assets", AssetsData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
