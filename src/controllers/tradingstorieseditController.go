package controllers

import (
	"net/http"
	"html/template"

	"gitlab.com/rene.sonntag/golang-tradingstories/src/models"
)

// DataTradingstoriesEdit : struct for tradingstoriesEdit
type DataTradingstoriesEdit struct {
	Title string
    Subtitle string
	ActiveMenu string
	FormNewMessage string
	FormNewMessageError string
	FormDeleteMessage string
	FormDeleteMessageError string
	Tradingstories []models.Tradingstories
	Assettypes []models.AssetTypes
	ID int64
	Name string
	Description string
	AssetTypeNames []models.AssetTypeNames
	Type string
	TrailingDistance int64
	TradingVolume string
}

// TradingstoriesEditHandler : handler for edit tradingstorie
func TradingstoriesEditHandler(w http.ResponseWriter, r *http.Request) {

	var TradingstoriesEditData DataTradingstoriesEdit

	// get tradingstorie data from form
	if r.Method == "POST" {

		// parse form
		r.ParseForm()

		if r.FormValue("submitEditTradingstorie") == "Speichern" {

			// save new tradingstorie
			editTradingstorieID := r.FormValue("tradingstoryid")
			editTradingstorieName := r.FormValue("name")
			editTradingstorieDescription := r.FormValue("description")
			editTradingstorieAssettypePid := r.Form["assettype_pid"]
			editTradingstorieType := r.FormValue("type")
			editTradingstorieTrailingDistance := r.FormValue("trailing_distance")
			editTradingstorieTradingVolume := r.FormValue("trading_volume")

			var updatedID int64

			if (editTradingstorieID != "" && editTradingstorieName != "" && editTradingstorieDescription != "" && editTradingstorieType != "" && editTradingstorieTrailingDistance != "" && editTradingstorieTradingVolume != "") {

				updatedID = models.UpdateTradingstories(editTradingstorieID, editTradingstorieName, editTradingstorieDescription, editTradingstorieAssettypePid, editTradingstorieType, editTradingstorieTrailingDistance, editTradingstorieTradingVolume)

				if (updatedID == 0) {

					// error
					TradingstoriesEditData.FormNewMessageError = "Fehler beim Speichern!"
				} else {
	
					// success
					TradingstoriesEditData.FormNewMessage = "Änderungen erfolgreich gespeichert."
				}
			} else {

				// error
				TradingstoriesEditData.FormNewMessageError = "Fehler: Pflichtfelder nicht befüllt!"
			}
		}

		// get tradingstory data from database
		tradingstoryByIDData := models.GetTradingstoryByID(r.FormValue("tradingstoryid"))
		TradingstoriesEditData.ID = tradingstoryByIDData[0].ID
		TradingstoriesEditData.Name = tradingstoryByIDData[0].Name
		TradingstoriesEditData.Description = tradingstoryByIDData[0].Description
		TradingstoriesEditData.AssetTypeNames = tradingstoryByIDData[0].AssetTypeNames
		TradingstoriesEditData.Type = tradingstoryByIDData[0].Type
		TradingstoriesEditData.TrailingDistance = tradingstoryByIDData[0].TrailingDistance
		TradingstoriesEditData.TradingVolume = tradingstoryByIDData[0].TradingVolume
	}
	
	// get assettypes from database
	TradingstoriesEditData.Assettypes = models.GetAssetTypes()

	// set meta data
	TradingstoriesEditData.Title = "TradingStories Bearbeiten"
	TradingstoriesEditData.Subtitle = "TradingStorie bearbeiten"
	TradingstoriesEditData.ActiveMenu = "tradingstoriesedit"

	var templates = template.Must(template.ParseGlob("templates/Bootstrapious/templates/*"))	

	err := templates.ExecuteTemplate(w, "tradingstoriesedit", TradingstoriesEditData)

	if err != nil {
		 http.Error(w, err.Error(), http.StatusInternalServerError)
		 return
	}
}
