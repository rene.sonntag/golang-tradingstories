package controllers

import (
	"html/template"
	"net/http"
	"strconv"

	"gitlab.com/rene.sonntag/golang-tradingstories/src/models"
)

// DataAssetTypes : struct for assettypes
type DataAssetTypes struct {
	Title                  string
	Subtitle               string
	ActiveMenu             string
	FormNewMessage         string
	FormNewMessageError    string
	FormDeleteMessage      string
	FormDeleteMessageError string
	ResultArray            []models.AssetTypes
}

// AssettypesHandler : handler for assettypes
func AssettypesHandler(w http.ResponseWriter, r *http.Request) {

	var AssetTypesData DataAssetTypes

	// get new assettypes from form
	if r.Method == "POST" {

		// parse form
		r.ParseForm()

		if r.FormValue("submitNewAssettype") == "Speichern" {

			// save new assettype
			newAssettypeName := r.FormValue("name")

			var insertedID int64

			if newAssettypeName != "" {

				insertedID = models.InsertNewAssetTypes(newAssettypeName)

				if insertedID == 0 {

					// error
					AssetTypesData.FormNewMessageError = "Fehler beim Anlegen oder Assettype bereits vorhanden!"
				} else {

					// success
					AssetTypesData.FormNewMessage = "Assettype mit der ID " + strconv.Itoa(int(insertedID)) + " erfolgreich angelegt."
				}
			} else {

				// error
				AssetTypesData.FormNewMessageError = "Fehler: Pflichtfelder nicht befüllt"
			}
		}

		if r.FormValue("submitDeleteAssettype") == "Löschen" {

			// delete assettype
			assettypeID := r.FormValue("assettypeId")

			var deletedID int64

			if assettypeID != "" {

				deletedID = models.DeleteAssetTypes(assettypeID)

				if deletedID == 0 {

					// error
					AssetTypesData.FormDeleteMessageError = "Fehler beim Löschen des Assettypes!"
				} else {

					// success
					AssetTypesData.FormDeleteMessage = "Assettype mit der ID " + assettypeID + " erfolgreich gelöscht."
				}
			} else {

				// error
				AssetTypesData.FormDeleteMessageError = "Fehler beim Löschen des Assettypes!"
			}
		}
	}

	// get assettypes from database
	AssetTypesData.ResultArray = models.GetAssetTypes()

	// set meta data
	AssetTypesData.Title = "AssetTypes"
	AssetTypesData.Subtitle = "Verwaltung von AssetTypes"
	AssetTypesData.ActiveMenu = "assettypes"

	var templates = template.Must(template.ParseGlob("templates/Bootstrapious/templates/*"))

	err := templates.ExecuteTemplate(w, "assettypes", AssetTypesData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
