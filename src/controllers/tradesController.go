package controllers

import (
	"html/template"
	"net/http"
	"strconv"

	"gitlab.com/rene.sonntag/golang-tradingstories/src/models"
)

// DataTrades : struct for trades
type DataTrades struct {
	Title                  string
	Subtitle               string
	ActiveMenu             string
	FormNewMessage         string
	FormNewMessageError    string
	FormDeleteMessage      string
	FormDeleteMessageError string
	TradingstoryID         int
	Tradingstories         []models.Tradingstories
	Trades                 []models.Trades
}

// Trades : title struct for trades
type Trades struct {
	Title string
}

// TradesHandler : handler for trades
func TradesHandler(w http.ResponseWriter, r *http.Request) {

	var TradesData DataTrades

	// get tradingstories from database
	TradesData.Tradingstories = models.GetTradingstories()

	// get trades for given tradingstory

	// parse form
	r.ParseForm()
	if r.FormValue("ts") != "" {

		tradingstoryID, _ := strconv.Atoi(r.FormValue("ts"))
		TradesData.Trades = models.GetTradesByTradingstoryID(tradingstoryID)
		TradesData.TradingstoryID = tradingstoryID
	}

	// set meta data
	TradesData.Title = "Trades"
	TradesData.Subtitle = "Trading Historie"
	TradesData.ActiveMenu = "trades"

	var templates = template.Must(template.ParseGlob("templates/Bootstrapious/templates/*"))

	err := templates.ExecuteTemplate(w, "trades", TradesData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
