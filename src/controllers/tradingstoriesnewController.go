package controllers

import (
	"html/template"
	"net/http"
	"strconv"

	"gitlab.com/rene.sonntag/golang-tradingstories/src/models"
)

// DataTradingstoriesNew : struct for tradingstoriesNew
type DataTradingstoriesNew struct {
	Title                  string
	Subtitle               string
	ActiveMenu             string
	FormNewMessage         string
	FormNewMessageError    string
	FormDeleteMessage      string
	FormDeleteMessageError string
	ResultArray            []models.Assets
	Assettypes             []models.AssetTypes
}

// TradingstoriesNewHandler : handler for new tradingstorie
func TradingstoriesNewHandler(w http.ResponseWriter, r *http.Request) {

	var TradingstoriesNewData DataTradingstoriesNew

	// get new tradingstorie from form
	if r.Method == "POST" {

		// parse form
		r.ParseForm()

		if r.FormValue("submitNewTradingstorie") == "Speichern" {

			// save new tradingstorie
			newTradingstorieName := r.FormValue("name")
			newTradingstorieDescription := r.FormValue("description")
			newTradingstorieAssettypePid := r.Form["assettype_pid"]
			newTradingstorieType := r.FormValue("type")
			newTradingstorieTrailingDistance := r.FormValue("trailing_distance")
			newTradingstorieTradingVolume := r.FormValue("trading_volume")

			var insertedID int64

			if newTradingstorieName != "" && newTradingstorieDescription != "" && newTradingstorieType != "" && newTradingstorieTrailingDistance != "" && newTradingstorieTradingVolume != "" {

				insertedID = models.InsertNewTradingstories(newTradingstorieName, newTradingstorieDescription, newTradingstorieAssettypePid, newTradingstorieType, newTradingstorieTrailingDistance, newTradingstorieTradingVolume)

				if insertedID == 0 {

					// error
					TradingstoriesNewData.FormNewMessageError = "Fehler beim Anlegen oder Tradingstorie bereits vorhanden!"
				} else {

					// success
					TradingstoriesNewData.FormNewMessage = "Tradingstorie mit der ID " + strconv.Itoa(int(insertedID)) + " erfolgreich angelegt."
				}
			} else {

				// error
				TradingstoriesNewData.FormNewMessageError = "Fehler: Pflichtfelder nicht befüllt!"
			}
		}
	}

	// get assettypes from database
	TradingstoriesNewData.Assettypes = models.GetAssetTypes()

	// set meta data
	TradingstoriesNewData.Title = "TradingStories Neu"
	TradingstoriesNewData.Subtitle = "Neue TradingStorie anlegen"
	TradingstoriesNewData.ActiveMenu = "tradingstoriesnew"

	var templates = template.Must(template.ParseGlob("templates/Bootstrapious/templates/*"))

	err := templates.ExecuteTemplate(w, "tradingstoriesnew", TradingstoriesNewData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
