package repositories

import (
	"github.com/piquette/finance-go"
	"github.com/piquette/finance-go/quote"
)

// APIQuote : struct for quotes got from API
type APIQuote struct {
	// Quote classifying fields.
	Symbol      string
	MarketState finance.MarketState
	QuoteType   finance.QuoteType
	ShortName   string

	// Regular session quote data.
	RegularMarketChangePercent float64
	RegularMarketPreviousClose float64
	RegularMarketPrice         float64
	RegularMarketTime          int
	RegularMarketChange        float64
	RegularMarketOpen          float64
	RegularMarketDayHigh       float64
	RegularMarketDayLow        float64
	RegularMarketVolume        int

	// Quote depth.
	Bid     float64
	Ask     float64
	BidSize int
	AskSize int

	// Pre-market quote data.
	PreMarketPrice         float64
	PreMarketChange        float64
	PreMarketChangePercent float64
	PreMarketTime          int

	// Post-market quote data.
	PostMarketPrice         float64
	PostMarketChange        float64
	PostMarketChangePercent float64
	PostMarketTime          int

	// 52wk ranges.
	FiftyTwoWeekLowChange         float64
	FiftyTwoWeekLowChangePercent  float64
	FiftyTwoWeekHighChange        float64
	FiftyTwoWeekHighChangePercent float64
	FiftyTwoWeekLow               float64
	FiftyTwoWeekHigh              float64

	// Averages.
	FiftyDayAverage                   float64
	FiftyDayAverageChange             float64
	FiftyDayAverageChangePercent      float64
	TwoHundredDayAverage              float64
	TwoHundredDayAverageChange        float64
	TwoHundredDayAverageChangePercent float64

	// Volume metrics.
	AverageDailyVolume3Month int
	AverageDailyVolume10Day  int

	// Quote meta-data.
	QuoteSource               string
	CurrencyID                string
	IsTradeable               bool
	QuoteDelay                int
	FullExchangeName          string
	SourceInterval            int
	ExchangeTimezoneName      string
	ExchangeTimezoneShortName string
	GMTOffSetMilliseconds     int
	MarketID                  string
	ExchangeID                string
}

// GetQuoteFromAPI : gets quotes from finance API
func GetQuoteFromAPI(symbol string) (response APIQuote, err bool) {

	var resultQuote APIQuote

	// get asset data from API
	if symbol != "" {

		// american stock market
		q, err := quote.Get(symbol)
		if err != nil {
			panic(err)
		}

		if q.Bid == 0 {
			// Frankfurt stock market ("Symbol.F")
			q, err = quote.Get(symbol + ".F")
			if err != nil {
				panic(err)
			}
		}

		// fmt.Println(q)

		/////////////////////////////////////////
		// mapping results
		// just in case the API has to be changed
		/////////////////////////////////////////

		// Quote classifying fields.
		resultQuote.Symbol = q.Symbol
		resultQuote.MarketState = q.MarketState
		resultQuote.QuoteType = q.QuoteType
		resultQuote.ShortName = q.ShortName

		// Regular session quote data.
		resultQuote.RegularMarketChangePercent = q.RegularMarketChangePercent
		resultQuote.RegularMarketPreviousClose = q.RegularMarketPreviousClose
		resultQuote.RegularMarketPrice = q.RegularMarketPrice
		resultQuote.RegularMarketTime = q.RegularMarketTime
		resultQuote.RegularMarketChange = q.RegularMarketChange
		resultQuote.RegularMarketOpen = q.RegularMarketOpen
		resultQuote.RegularMarketDayHigh = q.RegularMarketDayHigh
		resultQuote.RegularMarketDayLow = q.RegularMarketDayLow
		resultQuote.RegularMarketVolume = q.RegularMarketVolume

		// Quote depth.
		resultQuote.Bid = q.Bid
		resultQuote.Ask = q.Ask
		resultQuote.BidSize = q.BidSize
		resultQuote.AskSize = q.AskSize

		// Pre-market quote data.
		resultQuote.PreMarketPrice = q.PreMarketPrice
		resultQuote.PreMarketChange = q.PreMarketChange
		resultQuote.PreMarketChangePercent = q.PreMarketChangePercent
		resultQuote.PreMarketTime = q.PreMarketTime

		// Post-market quote data.
		resultQuote.PostMarketPrice = q.PostMarketPrice
		resultQuote.PostMarketChange = q.PostMarketChange
		resultQuote.PostMarketChangePercent = q.PostMarketChangePercent
		resultQuote.PostMarketTime = q.PostMarketTime

		// 52wk ranges.
		resultQuote.FiftyTwoWeekLowChange = q.FiftyTwoWeekLowChange
		resultQuote.FiftyTwoWeekLowChangePercent = q.FiftyTwoWeekLowChangePercent
		resultQuote.FiftyTwoWeekHighChange = q.FiftyTwoWeekHighChange
		resultQuote.FiftyTwoWeekHighChangePercent = q.FiftyTwoWeekHighChangePercent
		resultQuote.FiftyTwoWeekLow = q.FiftyTwoWeekLow
		resultQuote.FiftyTwoWeekHigh = q.FiftyTwoWeekHigh

		// Averages.
		resultQuote.FiftyDayAverage = q.FiftyDayAverage
		resultQuote.FiftyDayAverageChange = q.FiftyDayAverageChange
		resultQuote.FiftyDayAverageChangePercent = q.FiftyDayAverageChangePercent
		resultQuote.TwoHundredDayAverage = q.TwoHundredDayAverage
		resultQuote.TwoHundredDayAverageChange = q.TwoHundredDayAverageChange
		resultQuote.TwoHundredDayAverageChangePercent = q.TwoHundredDayAverageChangePercent

		// Volume metrics.
		resultQuote.AverageDailyVolume3Month = q.AverageDailyVolume3Month
		resultQuote.AverageDailyVolume10Day = q.AverageDailyVolume10Day

		// Quote meta-data.
		resultQuote.QuoteSource = q.QuoteSource
		resultQuote.CurrencyID = q.CurrencyID
		resultQuote.IsTradeable = q.IsTradeable
		resultQuote.QuoteDelay = q.QuoteDelay
		resultQuote.FullExchangeName = q.FullExchangeName
		resultQuote.SourceInterval = q.SourceInterval
		resultQuote.ExchangeTimezoneName = q.ExchangeTimezoneName
		resultQuote.ExchangeTimezoneShortName = q.ExchangeTimezoneShortName
		resultQuote.GMTOffSetMilliseconds = q.GMTOffSetMilliseconds
		resultQuote.MarketID = q.MarketID
		resultQuote.ExchangeID = q.ExchangeID

		return resultQuote, false
	}

	// empty result, error true
	return resultQuote, true
}
