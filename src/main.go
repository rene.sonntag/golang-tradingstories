package main

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/rene.sonntag/golang-tradingstories/src/controllers"
	"gitlab.com/rene.sonntag/golang-tradingstories/src/models"
)

type Data struct {
	Title      string
	Subtitle   string
	ActiveMenu string
}

func indexHandler(w http.ResponseWriter, r *http.Request) {

	page := &Data{Title: "Home", Subtitle: "Übersicht", ActiveMenu: "index"}
	renderTemplate(w, "index", page)
}

func configHandler(w http.ResponseWriter, r *http.Request) {

	page := &Data{Title: "Einstellungen", Subtitle: "Konfiguration des Tools", ActiveMenu: "config"}
	renderTemplate(w, "config", page)
}

func docuHandler(w http.ResponseWriter, r *http.Request) {

	page := &Data{Title: "Anleitung", Subtitle: "Wie funktioniert TradingStories?", ActiveMenu: "docu"}
	renderTemplate(w, "docu", page)
}

func main() {

	models.InitializeDatabase("root:root#41153@tcp(tradingstories-db:3306)/tradingstories")
	handleRequests()
}

func handleRequests() {

	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/tradingstories", controllers.TradingstoriesHandler)
	http.HandleFunc("/tradingstoriesnew", controllers.TradingstoriesNewHandler)
	http.HandleFunc("/tradingstoriesedit", controllers.TradingstoriesEditHandler)
	http.HandleFunc("/tradingstoriesmanuell", controllers.TradingstoriesManuellHandler)
	http.HandleFunc("/tradingstoriesmanuellsell", controllers.TradingstoriesManuellSellHandler)
	http.HandleFunc("/trades", controllers.TradesHandler)
	http.HandleFunc("/assets", controllers.AssetsHandler)
	http.HandleFunc("/assettypes", controllers.AssettypesHandler)
	http.HandleFunc("/config", configHandler)
	http.HandleFunc("/docu", docuHandler)
	http.HandleFunc("/testasset", controllers.TestAssetHandler)

	http.Handle("/vendor/", http.StripPrefix("/vendor/", http.FileServer(http.Dir("templates/Bootstrapious/vendor"))))
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("templates/Bootstrapious/css"))))
	http.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("templates/Bootstrapious/js"))))
	http.Handle("/fonts/", http.StripPrefix("/fonts/", http.FileServer(http.Dir("templates/Bootstrapious/fonts"))))
	http.Handle("/img/", http.StripPrefix("/img/", http.FileServer(http.Dir("templates/Bootstrapious/img"))))

	log.Println("Listen to port 80 ...")
	http.ListenAndServe(":80", nil)
}

func renderTemplate(w http.ResponseWriter, tmpl string, page *Data) {

	var templates = template.Must(template.ParseGlob("templates/Bootstrapious/templates/*"))

	err := templates.ExecuteTemplate(w, tmpl, page)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
