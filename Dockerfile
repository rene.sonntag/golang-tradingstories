# build stage
# FROM golang:1.13.6-alpine3.10 AS build-env
FROM golang:1.14.14-alpine3.13 AS build-env

# install some additional apps
RUN apk --no-cache add build-base git gcc

RUN mkdir -p /go/src/tradingstories

# Copy the module files first and then download the dependencies. If this
# doesn't change, we won't need to do this again in future builds.
COPY go.* /go/src/tradingstories/
WORKDIR /go/src/tradingstories
RUN go mod download

# Add some directories
WORKDIR /go/src/tradingstories
ADD src src

# Build application
WORKDIR /go/src/tradingstories/src
RUN go build -o tradingstories

# final stage
FROM alpine:latest
WORKDIR /app
RUN apk --no-cache update
RUN apk --no-cache upgrade
RUN apk --no-cache add tzdata
RUN cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime
RUN echo "Europe/Berlin" >  /etc/timezone

COPY --from=build-env /go/src/tradingstories/src/tradingstories /app/
COPY src/templates /app/templates
ENTRYPOINT ./tradingstories
