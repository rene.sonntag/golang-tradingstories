#
# Table structure for table 'asset_types'
#
CREATE TABLE asset_types (
  id int(11) unsigned NOT NULL auto_increment PRIMARY KEY,
  name varchar(255) DEFAULT NULL,
  description text DEFAULT NULL
);

#
# Table structure for table 'assets'
#
CREATE TABLE assets (
  id int(11) unsigned NOT NULL auto_increment PRIMARY KEY,
  assettype_pid int(11) unsigned DEFAULT NULL,
  name varchar(255) DEFAULT NULL,
  symbol varchar(50) DEFAULT NULL,
  disabled int(1) unsigned DEFAULT NULL
);

#
# Table structure for table 'trades'
#
CREATE TABLE trades (
  id int(11) unsigned NOT NULL auto_increment PRIMARY KEY,
  asset_pid int(11) UNSIGNED DEFAULT NULL,
  asset_name varchar(255) DEFAULT NULL,
  asset_symbol varchar(255) DEFAULT NULL,
  tradingstories_pid int(11) UNSIGNED DEFAULT NULL,
  amount int(11) UNSIGNED DEFAULT NULL,
  buy_date datetime DEFAULT NULL,
  buy_price varchar(50) DEFAULT NULL,
  buy_reason text DEFAULT NULL,
  sell_date datetime DEFAULT NULL,
  sell_price varchar(50) DEFAULT NULL,
  sell_reason text DEFAULT NULL,
  stopploss_percent float DEFAULT NULL
);

#
# Table structure for table 'tradingstories'
#
CREATE TABLE tradingstories (
  id int(11) unsigned NOT NULL auto_increment PRIMARY KEY,
  name text DEFAULT NULL,
  description text DEFAULT NULL,
  type varchar(50) DEFAULT NULL,
  trailing_distance float DEFAULT NULL,
  trading_volume_start float DEFAULT NULL,
  trading_volume float DEFAULT NULL,
  start_date datetime DEFAULT NULL,
  end_date datetime DEFAULT NULL,
  is_invested int(1) DEFAULT NULL
);

#
# Table structure for table 'tradingstories_assettypes_mm'
#
CREATE TABLE tradingstories_assettypes_mm (
  id int(11) unsigned NOT NULL auto_increment PRIMARY KEY,
  tradingstories_pid int(11) unsigned NOT NULL,
  assettypes_pid int(11) unsigned NOT NULL
);

#
# Table structure for table 'users'
#
CREATE TABLE users (
  id int(11) unsigned NOT NULL auto_increment PRIMARY KEY,
  surname varchar(100) DEFAULT NULL,
  name varchar(100) DEFAULT NULL,
  adress varchar(255) DEFAULT NULL,
  country varchar(50) DEFAULT NULL,
  zip varchar(10) DEFAULT NULL,
  phone varchar(50) DEFAULT NULL,
  email varchar(255) DEFAULT NULL
);
