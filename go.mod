module gitlab.com/rene.sonntag/golang-tradingstories

go 1.14

require (
	github.com/cespare/reflex v0.3.0 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/piquette/finance-go v1.0.0
	github.com/shopspring/decimal v1.2.0 // indirect
)
